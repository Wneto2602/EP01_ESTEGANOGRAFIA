#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <iostream>     // std::cin, std::cout   // std::ifstream
#include <string>
#include <bitset>
#include <cstring>
#include <cstdlib>
#include <istream>

using namespace std;	
class Imagem{

private:
	string nome;

public:
	Imagem();
	Imagem(string nome);
	virtual ~Imagem();
	string getNome();
	 void digiteNome();
protected:
	void setNome(string nome);
};
#endif
