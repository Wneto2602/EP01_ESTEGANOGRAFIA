#include "imagemPGM.hpp"
#include <string>
#include <iostream>     // std::cin, std::cout
#include <fstream>      // std::ifstream
#include <string>
#include <bitset>
#include <cstring>
#include <cstdlib>
#include <istream>

using namespace std;
PGM::PGM() {
  setNome(" ");
};
PGM::PGM(string nome){
  setNome(nome);
};
PGM::~PGM(){
}
//==========================
void PGM::calculaTamanhoCabecalho(){
  int tamanhocabecalho=0;
  int j=0,contador=0;
  char c;
   ifstream arquivo (getNome().c_str());
   for(j=0;j<1000;j++){
          arquivo.get(c);
          if(contador==4){
            break;
          }
          if(c=='\n'){
            contador++;
            tamanhocabecalho++;
          }else{
            tamanhocabecalho++;
          }
      }

      arquivo.close();
      this->tamanhocabecalho=tamanhocabecalho;
};
int PGM::getTamanhoCabecalho(){
  return this->tamanhocabecalho;
};
void PGM::setTamanhoCabecalho(int tamanhocabecalho){
  this->tamanhocabecalho=tamanhocabecalho;
};
//===============================
void PGM::decifrarMensagem(){
  int i=0,loop=0;

  char c;
  char chave[10];
  char simbolo[20];
  char posicao[10];
  int posicaoInicial=0;
  FILE *arquivo;
  digiteNome();
  ifstream myfile (getNome().c_str()); // ifstream = padrão ios:in
  arquivo=fopen(getNome().c_str(),"r");

    if (myfile.is_open())
    {

        fscanf(arquivo,"%s %s %s",chave,simbolo,posicao);
        posicaoInicial=atoi(posicao);    

        calculaTamanhoCabecalho();      
        while(! myfile.eof() ){
          myfile.seekg(posicaoInicial+getTamanhoCabecalho());
          char caracter_extraido;
          char bit_extraido;
          int bit = 2;

          for(i=0;i<8;i++) {
          myfile.get(c);
          bit_extraido =  c;
          bit = bit_extraido & 0x01;
          caracter_extraido = caracter_extraido << 1;     
          caracter_extraido = (caracter_extraido & 0xFE) | (bit_extraido & 0X01);
        }

      char caracter;
      caracter = caracter_extraido & 0xff;
       if(caracter=='#' && loop>0){
        break;
        }
      cout <<caracter<< "";
      caracter_extraido = 0;
      posicaoInicial += 8;
      loop++;
      
      } 
  }
   
    fclose(arquivo);
    myfile.close();
}



