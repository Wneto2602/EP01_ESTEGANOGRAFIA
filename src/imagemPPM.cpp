#include "imagemPPM.hpp"
#include <string>
#include <iostream>     
#include <string>
#include <bitset>
#include <cstring>
#include <cstdlib>
#include <istream>

using namespace std;

PPM::PPM() {
  setNome(" ");
};
PPM::PPM(string nome){
  setNome(nome);
};
PPM::~PPM(){
}
//========================================================
void PPM::calculaTamanhoCabecalho(){
  int tamanhocabecalho=0;
  int j=0,contador=0;
  char c;
   ifstream arquivo (getNome().c_str());
   for(j=0;j<1000;j++){
          arquivo.get(c);
          if(contador==4){
            break;
          }
          if(c=='\n'){
            contador++;
            tamanhocabecalho++;
          }else{
            tamanhocabecalho++;
          }
      }

      arquivo.close();
      this->tamanhocabecalho=tamanhocabecalho;
};
int PPM::getTamanhoCabecalho(){
  return this->tamanhocabecalho;
};
void PPM::setTamanhoCabecalho(int tamanhocabecalho){
  this->tamanhocabecalho=tamanhocabecalho;
};
//========================================================
void PPM::aplicaFiltroBlue(){
  char c;
  char chave[2];
  char comentario[50];
  char largura1[10];
  char altura1[10];
  char maximo1[10];
  int largura=0,altura=0,maximo=0;
  FILE *arquivo;
    digiteNome();
  ifstream myfile (getNome().c_str()); 
  arquivo=fopen(getNome().c_str(),"r");
    if (myfile.is_open())
    {
        fscanf(arquivo,"%s %s %s %s %s",chave,comentario,largura1,altura1,maximo1);
        largura=atoi(largura1);
        altura=atoi(altura1);
        maximo=atoi(maximo1);

  calculaTamanhoCabecalho();
  fstream arquivo_azul; 
  arquivo_azul.open("PPM.azul",ios::binary|ios::out);
  if (!arquivo_azul.is_open()) {
    cout<<"Erro, não foi possível abrir arquivo.";
  }
  else{
      int contador1=1;
      arquivo_azul<<"P6"<<"\n"<<largura<<" "<<altura<<'\n'<<maximo<<endl;
      myfile.seekg(getTamanhoCabecalho());
      while(! myfile.eof() )
      {
        myfile.get(c);
          if (contador1!=3){
            arquivo_azul<<0;
            contador1++;          
          }else{
            arquivo_azul<<c;
            contador1=1;
          }
      }
      cout<<"\nA aplicação do filtro AZUL foi salva na pasta principal do arquivo! "<<endl;
      arquivo_azul.close();
    }
     myfile.close();
   }    
    fclose(arquivo); 
};
//=================================================================================
void PPM::aplicaFiltroRed(){
  char c;
  char chave[2];
  char comentario[50];
  char largura1[10];
  char altura1[10];
  char maximo1[10];
  int largura=0,altura=0,maximo=0;
  FILE *arquivo;
    digiteNome();
  ifstream myfile (getNome().c_str()); // ifstream = padrão ios:in
  arquivo=fopen(getNome().c_str(),"r");
    if (myfile.is_open())
    {
        //PEGA NUMERO EQUIVALENTE A POSICAO INICIAL
        fscanf(arquivo,"%s %s %s %s %s",chave,comentario,largura1,altura1,maximo1);
        largura=atoi(largura1);
        altura=atoi(altura1);
        maximo=atoi(maximo1);

calculaTamanhoCabecalho();
fstream arquivo_vermelho; 
  arquivo_vermelho.open("PPM.vermelho",ios::binary|ios::out);
  if (!arquivo_vermelho.is_open()) {
    cout<<"Erro, não foi possível abrir arquivo.";
  }
  else{
      int contador1=1;
      arquivo_vermelho<<"P6"<<"\n"<<largura<<" "<<altura<<'\n'<<maximo<<endl;
      myfile.seekg(getTamanhoCabecalho());
      while(! myfile.eof() )
      {
        myfile.get(c);
          if (contador1!=1){
            arquivo_vermelho<<0;
            contador1++;          
          
          if (contador1==4){
          contador1=1;
            }
          }
            else{
            arquivo_vermelho<<c;
            contador1++;
          }
      }
  
      cout<<"\nA aplicação do filtro VERMELHO foi salva na pasta principal do arquivo!  "<<endl;
      arquivo_vermelho.close();
        
    }
       myfile.close();
  }
   
    fclose(arquivo);
};
//====================================================================================================
void PPM::aplicaFiltroGreen(){
  char c;
  char chave[2];
  char comentario[50];
  char largura1[10];
  char altura1[10];
  char maximo1[10];
  int largura=0,altura=0,maximo=0;
  FILE *arquivo;
  digiteNome();
  ifstream myfile (getNome().c_str()); // ifstream = padrão ios:in
  arquivo=fopen(getNome().c_str(),"r");
    if (myfile.is_open())
    {
        //PEGA NUMERO EQUIVALENTE A POSICAO INICIAL
        fscanf(arquivo,"%s %s %s %s %s",chave,comentario,largura1,altura1,maximo1);
        largura=atoi(largura1);
        altura=atoi(altura1);
        maximo=atoi(maximo1);
  
calculaTamanhoCabecalho();
 fstream arquivo_verde; 
  arquivo_verde.open("PPM.verde",ios::binary|ios::out);
  if (!arquivo_verde.is_open()) {
    cout<<"Erro, não foi possível abrir arquivo.";
  }
  else{
      int contador1=1;
      arquivo_verde<<"P6"<<"\n"<<largura<<" "<<altura<<'\n'<<maximo<<endl;
      myfile.seekg(getTamanhoCabecalho());
      while(! myfile.eof() )
      {
        myfile.get(c);
          if (contador1!=2){
            arquivo_verde<<0;
            contador1++;          
          
          if (contador1==4){
          contador1=1;
            }
          }
            else{
            arquivo_verde<<c;
            contador1++;
          }
      }
    
      cout<<"\nA aplicação do filtro VERDE foi salva na pasta principal do arquivo!  "<<endl;
      arquivo_verde.close();
   
    }
    myfile.close();

}
 
    fclose(arquivo);
 
};